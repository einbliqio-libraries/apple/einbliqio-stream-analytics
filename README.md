# EinbliqIO Stream Analytics Library

The `EinbliqIO` library measures the quality of service for video and audio playback sessions.

In order to collect quality of service playback analytics data, the library needs to be integrated into the app first and then has to be coupled with an `AVPlayer` instance.

After integrating and coupling the library, it will send analytics data to the einbliq.io servers for each playback session of a video or audio stream.

## Requirements

The library requires iOS/tvOS 12.4, visionOS 1 or higher to run.

## Installation

### Swift Package Manager

You can add the library to your project by selecting *File* -> *Swift Packages* -> *Add Package Dependency...* from the Menu Bar.

Enter `https://@gitlab.com/einbliqio-libraries/apple/einbliqio-stream-analytics.git` as the Package Repository URL and `1.3.0` as Version.

## Usage

### Coupling `EinbliqIOAdapter` with `AVPlayer`

The `EinbliqIOAapter` is initialized using an instance of `AVPlayer` as well as an `EinbliqIOSessionConfigurationSource`.

The adpater will automatically begin to observe the player and its current item. Each time the `AVPlayer` instance's current item changes (as well as once for the initial item), the adapter will call the `EinbliqIOSessionConfigurationSource` to provide the adapter with a valid `SessionConfiguration`. If no `SessionConfiguration` is provided, a playback session will not be started for that item.

#### Sample

```swift
let player = AVPlayer()
let adapter = EinbliqIOAdapter(player: player, configurationSource: self)
```

### The `SessionConfiguration`

The `SessionConfiguration` type holds static information about a session such as its media URL or any other information about the session. It's initialized using your einbliq.io customer ID as well as the media's URL.

| Param | Type | Description |
| --- | --- | --- |
| customerID | <code>String</code> | customerId. Provided by team@einbliq.io |
| mediaURL | <code>String</code> | mediaUrl |
| mediaID | <code>String?</code> | Recommended. Identifier of media asset |
| mediaTitle | <code>String?</code> | Recommended: Title of media asset |
| mediaSeries | <code>String?</code> | Recommended: Series of media asset |
| mediaCategory | <code>String?</code> | Recommended: Category of media asset |
| termID | <code>String?</code> | Recommended: termId of media asset |
| crid | <code>String?</code> | Recommended: CRID of media asset |
| customData | <code>[String: Encodable]?</code> | Optional: Custom Data. Can be any additional data that should be tracked alongside with playback metrics. All `objects` should be primitive values that can be parsed to JSON. |
| isAutoPreload | <code>Boolean</code> | Optional: Auto Preload Flag. Should be set to `true` if player auto preloads content before playback. Default: `false` |
| applicationName | <code>String?</code> | Recommended: Application Name |
| applicationVersion | <code>String?</code> | Recommended: Application Version |

#### Sample

```swift
class SessionConfiguration {
	/// customerID: provided by team@einbliq.io
	let customerID: String

	/// The session's media `URL`
	let mediaURL: URL

	// Optional
	var mediaID: String?
	var mediaTitle: String?
	var mediaSeries: String?
	var mediaCategory: String?
	var termID: String?
	var crid: String?
	var customData: [String: Encodable]?
	var isAutoPreload: Bool = false

	/// Defaults to `CFBundleDisplayName` key in the app's Info.plist at initialization.
	var applicationName: String?

	/// Defaults to `CFBundleShortVersionString` key in the app's Info.plist at initialization
	var applicationVersion: String?
}
```

It is initialized using the `init(customerID: String, mediaURL: URL) throws` constructor.

#### Sample

```swift
guard let configuration = try? SessionConfiguration(customerID: "xxxxxxxx", mediaURL: <some url>) else {
	return
}

configuration.mediaTitle = "My Show"
configuration.customData = [
	"Season": 5,
	"Episode": 20
]
```

### Providing `EinbliqIOAdapter` with `SessionConfigurations`

To provide your `EinbliqIOAdapter` instance with `SessionConfigurations` you need to implement the `EinbliqIOSessionConfigurationSource` protocol. The protocol only required one method:

```swift
public protocol EinbliqIOSessionConfigurationSource {
	func einbliqIOSessionConfiguration(_ adapter: EinbliqIOAdapter, for item: AVPlayerItem) -> SessionConfiguration?
}
```

#### Sample Implementation

```swift
extension ViewController: EinbliqIOSessionConfigurationSource {
	func einbliqIOSessionConfiguration(_ adapter: EinbliqIOAdapter, for item: AVPlayerItem) -> SessionConfiguration? {
		guard let url = (item.asset as? AVURLAsset)?.url else {
			// A valid media URL is needed for tracking, if none is available, we can't open a session for the item 
			return nil
		}

		guard let configuration = try? SessionConfiguration(customerID: "xxxxxxxx", mediaURL: url) else {
		    // This can fail if there's an issue with the Customer ID
		    // In this case, we can't open a session either
			return nil
		}
		
		return configuration
	}
}
```

### Opting Out

Simply call the `optOut()` method on the `EinbliqIOAdapter` instance to opt out of sending data. This method marks the instance unusable and a new one will have to be initialized if you need to send more data.

### Additional Notes

* The adapter holds a __strong__ reference to its `EinbliqIOSessionConfigurationSource` instance and the `AVPlayer` instance. Please consider this to avoid memory leaks.
