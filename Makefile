SWIFT_FORMAT := $(shell xcrun --find swift-format)

format:
	@echo "🎨 Formatting Code"
	$(SWIFT_FORMAT) format --in-place --recursive .

.PHONY: format
