// swift-tools-version:5.10

import PackageDescription

let package = Package(
  name: "EinbliqIO",
  platforms: [
    .iOS(.v12),
    .tvOS(.v12),
    .visionOS(.v1),
  ],
  products: [
    .library(
      name: "EinbliqIO",
      targets: ["EinbliqIO"]
    )
  ],
  targets: [
    .binaryTarget(
      name: "EinbliqIO",
      path: "./bin/EinbliqIO.xcframework"
    )
  ]
)
